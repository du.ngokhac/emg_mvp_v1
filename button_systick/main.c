#include <stdbool.h>
#include <stdint.h>
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "boards.h"      
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"                          

#define SYSTICK_COUNT_ENABLE        1
#define SYSTICK_INTERRUPT_ENABLE    2

// phan bien do minh viet
#define SHUTDOWN    		19	
#define LED_POWER   		4
#define LED_STATUS_PIN          3
#define BUTTON   		26


#define waitTime  2000 // B?n c?n nh?n gi? 500 mili gi�y d? h? th?ng xem d� l� m?t s? ki?n nh?n gi?.

bool ledStatus = 0; // tuong t? v?i LOW - m?c d?nh d�n s? t?tza
uint8_t count = 0;
bool lastButtonStatus = 0; //Bi?n d�ng d? luu tr?ng th�i c?a ph�m b?m. M?c d?nh l� 0 <=> LOW <=> chua nh?n
bool buttonLongPress = 0; // M?c d?nh l� kh�ng c� s? ki?n nh?n gi?.
int buttonShort = 0;
unsigned long lastChangedTime;// can dnh nghia ro rang
uint32_t msTicks = 0; /* Variable to store millisecond ticks */

static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

uint32_t NRF_GetTick(void)
{
  return msTicks;
}
void SysTick_Handler(void)
{
  msTicks++;
}

void gpio_init()
  {
        nrf_gpio_cfg_input(BUTTON, NRF_GPIO_PIN_PULLDOWN);
        nrf_gpio_cfg_output(LED_POWER);
        nrf_gpio_cfg_output(SHUTDOWN);	
        nrf_gpio_pin_clear(SHUTDOWN);//set bat nguon
  }
 #define  LOW  0
 #define  HIGH 1
const int DEBOUNCE_DELAY= 50;
bool preState = LOW; // bien luu trang thai truoc day 
bool currentState; //bien trang thai hien tai
bool lastAfterFilterNoiseState = HIGH;
unsigned long lastDebounceTime;
unsigned char state;
void Button_state()
{
state = 0;
currentState = nrf_gpio_pin_read(BUTTON);
if(currentState != lastAfterFilterNoiseState)
{
lastDebounceTime = NRF_GetTick();
lastAfterFilterNoiseState = currentState;
}
if ((NRF_GetTick() - lastDebounceTime) > DEBOUNCE_DELAY)
  {
      if(preState == LOW && currentState == HIGH)
        {
          state=1; // nhan
          NRF_LOG_INFO("press short nhan ");
          //preState = currentState ;
        }
        else
        {
          //Do Nothing
        }
      if(preState == HIGH )
        {
          if(currentState == LOW)
          {
              //preState = currentState;
              NRF_LOG_INFO("nha");

          }
          else
          {         
              if( (NRF_GetTick() - lastDebounceTime) > 2000 )
              {   
                  state=2;//giu
                  NRF_LOG_INFO("press long ");
                  nrf_gpio_pin_toggle(SHUTDOWN);
               
              }        
          }
        }
       else
        {
          //Do Nothing
        }
      preState = currentState;
  }
    else
  {
    //Do Nothing
  }
  }
//void button_init()
//  {
//        bool reading = nrf_gpio_pin_read(BUTTON); // d?c gi� tr? c?a button
//        if (reading != lastButtonStatus) { // N?u b?n dang nh?n button r?i th? ra gi?a ch?ng ho?c dang th? button r?i nh?n l?i 
//                                         // th� ta c?n ph?i c?p nh?p l?i th?i gian thay d?i cu?i c�ng
//          count++;
//          NRF_LOG_INFO("press short %d",count);
//          lastButtonStatus = reading; //C?p nh?p tr?ng th�i cu?i c�ng.
//          lastChangedTime = NRF_GetTick(); //C?p nh?p th?i gian
//        } // C�n n?u b?n dang nh?n gi? button ho?c th? n� th?i gian d�i ch? s? kh�ng ?nh hu?ng d?n vi?c n�y
//        if (NRF_GetTick() - lastChangedTime > waitTime) 
//           {
//                nrf_gpio_pin_set(LED_POWER);
//                // N?u s? ch�nh l?ch gi?a th?i di?m dang x�t v� th?i di?m cu?i c�ng thay d?i tr?ng th�i c?a button l?n hon th?i gian d?i d? t?o s? ki?n nh?n gi? TH� n� l� m?t s? ki?n nh?n gi?
//                buttonLongPress = reading; // C?p nh?p tr?ng th�i c?a button = v?i tr?ng th�i c?a button
//               // NRF_LOG_INFO("press longggggg %d",buttonLongPress);
//                lastChangedTime = NRF_GetTick();
//           }
  
//        if (buttonLongPress == true) 
//          {                 
//              buttonLongPress = false; // K?t th�c s? ki?n nh?n gi?
//              if(buttonLongPress==false)
//               {
//                   NRF_LOG_INFO("press long");
//                   nrf_gpio_pin_toggle(SHUTDOWN);
//               }
//           }

//  }

int main(void)
{
    gpio_init();
    uint32_t time = NRF_GetTick();
    uint32_t returnCode;
    returnCode = SysTick_Config(SystemCoreClock / 1000);      /* Configure SysTick to generate an interrupt every millisecond */ 
    if (returnCode != 0)  {                                   /* Check return code for errors */
    }
    log_init();
    while(1)
    {
        //button_init();
        Button_state();
    }
}